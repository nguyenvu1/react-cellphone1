import React, { Component } from "react";
import { connect } from "react-redux";

class ProductItem extends Component {
  handleAddToCart = (
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc
  ) => {
    this.props.dispatch({
      type: "SET_ADD_TO_CART",
      payload: {
        id,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        quantity: 1,
      },
    });
  };
  handleSelectedProduct = (
    id,
    name,
    price,
    screen,
    backCamera,
    frontCamera,
    img,
    desc
  ) => {
    this.props.dispatch({
      type: "SET_SELECTED_PRODUCT",
      payload: {
        id,
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
      },
    });
  };
  render() {
    const { id, name, price, screen, backCamera, frontCamera, img, desc } =
      this.props.prod;
    return (
      <div className="card">
        <img style={{ height: 250, width: "100%" }} src={img} alt="product" />
        <div className="card-body">
          <h4>{name}</h4>
          <button
            onClick={() =>
              this.handleSelectedProduct(
                id,
                name,
                price,
                screen,
                backCamera,
                frontCamera,
                img,
                desc
              )
            }
            className="btn btn-info"
          >
            Chi tiết
          </button>
          <button
            onClick={() =>
              this.handleAddToCart(
                id,
                name,
                price,
                screen,
                backCamera,
                frontCamera,
                img,
                desc
              )
            }
            className="btn btn-danger"
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

export default connect()(ProductItem);
